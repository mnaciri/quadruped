import argparse
import math
import numpy as np
from time import sleep
import pybullet as p

dt=0.01



def init():
    physicsClient=p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    planeId=p.loadURDF('plane.urdf')


    startPos=[0, 0, 0.1]
    startOrientation=p.getQuaternionFromEuler([0, 0, 0])
    robot=p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)
    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    jointsMap=[0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo=p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def demo(t, amplitude):
    joints=[0]*12
    joints[0]=math.sin(t) * amplitude
    return joints


def interpolate(xs, ys, x):
    if (x>xs[len(xs)-1]):
        return ys[len(ys)-1]
    if (x<=xs[0]):
        return ys[0]
    for t in range(len(xs)):
        if (x<xs[t]):
            a=(ys[t]-ys[t-1])/(xs[t]-xs[t-1])
            b=ys[t]
            return a*(x-xs[t])+b

def cinematiqueInverse(x,y,z):
    #choix des longeurs en mètre :-)
    l1=0.045
    l2=0.065
    l3=0.087
    theta0=math.atan2(y,x)
    dist=math.sqrt((x-l1*math.cos(theta0))**2+(y-l1*math.sin(theta0))**2+z**2)
    if (dist==0):
        dist=0.001
    longeurs=[]
    longeurs.append((l2**2+dist**2-l3**2)/(2*dist*l2))
    longeurs.append(z/dist)
    longeurs.append((l2**2+l3**2-dist**2)/(2*l2*l3))
    for i in range (len(longeurs)):
        if (longeurs[i]>1):
            longeurs[i]=1
        if (longeurs[i]<-1):
            longeurs[i]=-1
    return [theta0, math.acos(longeurs[0])+math.asin(longeurs[1]), (-math.acos(longeurs[2]))-math.pi]

def leg_ik():
    joints=[0]*12
    a=cinematiqueInverse(p.readUserDebugParameter(x0),p.readUserDebugParameter(y0),p.readUserDebugParameter(z0))
    b=cinematiqueInverse(p.readUserDebugParameter(x1),p.readUserDebugParameter(y1),p.readUserDebugParameter(z1))
    c=cinematiqueInverse(p.readUserDebugParameter(x2),p.readUserDebugParameter(y2),p.readUserDebugParameter(z2))
    d=cinematiqueInverse(p.readUserDebugParameter(x3),p.readUserDebugParameter(y3),p.readUserDebugParameter(z3))
    for i in range(12):
        if (i<3):
            joints[i]=a[i]
        if ((i>=3) & (i<6)):
            joints[i]=b[i%3]
        if ((i>=6) & (i<9)):
            joints[i]=c[i%3]
        if ((i>=9) & (i<12)):
            joints[i]=d[i%3]
    return joints


def mooveLeg(legId,t,pos,T,joints):
    t=math.fmod(t,T[len(T)-1])
    newX=interpolate(T,pos[0],t)
    newY=interpolate(T,pos[1],t)
    newZ=interpolate(T,pos[2],t)
    values=cinematiqueInverse(newX,newY,newZ)
    for i in range(len(values)):
        joints[legId*3+i]=values[i]
    return joints


def immobile(t):
    joints=[0]*12
    for i in range(4):
        joints[i*3]=0
        joints[i*3+1]=(math.pi/4)
        joints[i*3+2]=3*(math.pi/4)
    return joints

def computeIKOriented(legId,x,y,z,extra_theta=0,pos=[0.09,0.0,-0.045]):#pos=[2*l1,0,-l1]
    theta=legId*math.pi/2 + math.pi/4
    xPatte=(pos[0]*math.cos(extra_theta))+(pos[1]*math.sin(extra_theta))-(x*math.cos(theta))-(y*math.sin(theta))
    yPatte=(pos[1]*math.cos(extra_theta))-(pos[0]*math.sin(extra_theta))-(y*math.cos(theta))+(x*math.sin(theta))
    zPatte=pos[2]-z
    return [xPatte,yPatte,zPatte]

def robot_ik():
    joints=[0]*12
    for i in range (4):
        values=computeIKOriented(i,p.readUserDebugParameter(x),p.readUserDebugParameter(y),p.readUserDebugParameter(z))
        cinValues=cinematiqueInverse(values[0],values[1],values[2])
        for j in range(len(cinValues)):
            joints[i*3+j]=cinValues[j]
    return joints

def walk(t,xSpeed,ySpeed,thetaSpeed):
    T=[0,0.5*0.5,1.5*0.5,2*0.5,3*0.5]
    xPatte,yPatte,zPatte=[0]*4,[0]*4,[0]*4
    depx = xSpeed*3*0.5/4
    depy = ySpeed*3*0.5/4
    for i in range(4):
        if (i%2==0):
            x=[0]*5
            y=[0]*5
            z=[0]*5
            x[0],y[0],z[0]=computeIKOriented(i,-depx,-depy,-0.005,-thetaSpeed*0.5*0.5)
            x[1],y[1],z[1]=computeIKOriented(i,-depx,-depy,0.005,-thetaSpeed*1.5*0.5)
            x[2],y[2],z[2]=computeIKOriented(i,depx,depy,0.005,thetaSpeed*0.5*0.5)
            x[3],y[3],z[3]=computeIKOriented(i,depx,depy,-0.005,thetaSpeed*1.5*0.5)
            x[4],y[4],z[4]=x[0],y[0],z[0]
            xPatte[i]=x
            yPatte[i]=y
            zPatte[i]=z
        else:
            x=[0]*5
            y=[0]*5
            z=[0]*5
            x[0],y[0],z[0]=computeIKOriented(i,depx,depy,0.005,thetaSpeed*0.5*0.5)
            x[1],y[1],z[1]=computeIKOriented(i,depx,depy,-0.005,thetaSpeed*1.5*0.5)
            x[2],y[2],z[2]=computeIKOriented(i,-depx,-depy,-0.005,-thetaSpeed*1*0.5)
            x[3],y[3],z[3]=computeIKOriented(i,-depx,-depy,0.005,-thetaSpeed*1.5*0.5)
            x[4],y[4],z[4]=x[0],y[0],z[0]
            xPatte[i]=x
            yPatte[i]=y
            zPatte[i]=z
    joints = [0]*12
    for i in range (4):
        joints[i*3:3+i*3] = mooveLeg(i,t,[xPatte[i],yPatte[i],zPatte[i]],T,joints)[i*3:3+i*3]
    return joints


def goto(t,x,y,theta):
    speed=0.1
    thetaSpeed=-math.pi/6
    dist=math.sqrt(x**2+y**2)
    if (dist>0):
        xSpeed=(x/dist)*speed
        ySpeed=(y/dist)*speed
    if (t<dist/speed):
        return walk(t,xSpeed,ySpeed,0)
    elif (t<((dist/speed)+(theta/thetaSpeed))):
        return walk(t,0,0,thetaSpeed)
    else:
        return immobile(t)

def collisionCorrection(joints):
    for i in range(4):
        if (joints[i*3]>math.pi/2+joints[((i+1)*3)%12]):
            joints[i*3]=math.pi/2+joints[((i+1)*3)%12]
    jointsCorrected=[0]*12
    jointsCorrected[0:9]=joints[3:]
    jointsCorrected[9:12]=joints[0:3]
    return jointsCorrected

def gotoFun(t,x,y,theta):
    speed=3
    thetaSpeed=math.pi/6
    dist=math.sqrt(x**2+y**2)
    if (dist>0):
        xSpeed=(x/dist)*speed
        ySpeed=(y/dist)*speed
    if (t<dist/speed):
        return walk(t,xSpeed,ySpeed,0)
    elif (t<(dist/speed + theta/thetaSpeed)):
        return walk(t,0,0,thetaSpeed)
    else:
        return immobile(t)

def fun(t,x,y,theta):
    return gotoFun(t,x,y,theta)

if __name__ == "__main__":
    parser=argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-theta', type=float, help='Theta target for goto (rad)', default=0.0)
    args=parser.parse_args()

    mode, x, y, theta=args.m, args.x, args.y, args.theta

    if mode not in ['demo', 'leg_ik', 'robot_ik','walk', 'goto', 'fun','immobile']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot=init()
    if (mode=='demo'):
        amplitude=p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif (mode=='leg_ik'):
        x0=p.addUserDebugParameter("x0", 0.0, 0.200, 0.1)
        y0=p.addUserDebugParameter("y0", -0.20, 0.20, 0)
        z0=p.addUserDebugParameter("z0", -0.20, 0.20, -0.05)
        x1=p.addUserDebugParameter("x1", 0.0, 0.200, 0.1)
        y1=p.addUserDebugParameter("y1", -0.20, 0.20, 0)
        z1=p.addUserDebugParameter("z1", -0.20, 0.20, -0.05)
        x2=p.addUserDebugParameter("x2", 0.0, 0.200, 0.1)
        y2=p.addUserDebugParameter("y2", -0.20, 0.20, 0)
        z2=p.addUserDebugParameter("z2", -0.20, 0.20, -0.05)
        x3=p.addUserDebugParameter("x3", 0.0, 0.200, 0.1)
        y3=p.addUserDebugParameter("y3", -0.20, 0.20, 0)
        z3=p.addUserDebugParameter("z3", -0.20, 0.20, -0.05)
    elif (mode=='robot_ik'):
        x=p.addUserDebugParameter("x", -0.10, 0.10, 0)
        y=p.addUserDebugParameter("y", -0.10, 0.10, 0)
        z=p.addUserDebugParameter("z", -0.20, 0.20, 0)
    elif (mode=='walk'):
        x_speed=p.addUserDebugParameter("x_speed", -0.3, 0.3, 0)
        y_speed=p.addUserDebugParameter("y_speed", -0.3, 0.3, 0)
        t_speed=p.addUserDebugParameter("t_speed", -math.pi/2, math.pi/2, 0)
    elif (mode=='goto'):
        x, y, theta, args.y, args.theta
    t=0
    while True:
        t += dt
        if (mode=='demo'):
            joints=demo(t, p.readUserDebugParameter(amplitude))
        if (mode=='leg_ik'):
            joints=leg_ik()
        if (mode=='immobile'):
            joints=immobile(t)
        if (mode=='robot_ik'):
            if (t<2):
                joints=immobile(t)
            else:
                joints=robot_ik()
        if (mode=='walk'):
            if (t<2):
                joints=immobile(t)
            else:
                joints=walk(t-2,p.readUserDebugParameter(x_speed),p.readUserDebugParameter(y_speed),p.readUserDebugParameter(t_speed))
        if (mode=='goto'):
            if (t<2):
                joints=immobile(t)
            else:
                joints=goto(t-2,1.0,1.0,theta)
        if (mode=='fun'):
            if (t<2):
                joints=immobile(t)
            else:
                joints=fun(t-2,x,y,theta)
        joints=collisionCorrection(joints)
        setJoints(robot, joints)
        p.stepSimulation()
        sleep(dt)